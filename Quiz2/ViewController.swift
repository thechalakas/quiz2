//
//  ViewController.swift
//  Quiz2
//
//  Created by Jay on 25/07/17.
//  Copyright © 2017 the chalakas. All rights reserved.
//

/*
 
 Some rants will come here. 
 
 See below where the brackets open right after the name of the method, for example, ViewController
 got, it is so frustrating. why not use the standard stuff where the brackets come after the name :(
 
 
 
 */

import UIKit

class ViewController: UIViewController
{

    //lets create outlets that will go refer a view object
    
    //now, the animation, only the first tap works 
    //this is because our animation is dependent on the opacity alpha value, which is set to 0 only at the beginning of the app launch
    //when the animation completes, the alpha value is already 1, so even though the animation does happen, it is happening from
    //100 % to 100 % opacity, thereby nothing for the user to see
    
    //as a solution, what we do is we have two question labels, each with alternating alpha opacity values
    //that way, when one becomes 100. the other becomes 0 and vice versa, allowing for animation each time the next question button is pressed.
    
    //so, lets stop using the questionLabel below.
    
    //@IBOutlet var questionLabel: UILabel!
    
    //lets create two new labels for the questions
    @IBOutlet var currentQuestionLabel: UILabel!
    @IBOutlet var nextQuestionLabel: UILabel!
    
    @IBOutlet var answerLabel: UILabel!
    
    //we need some models. I mean, we already have the View (Storyboard), Controller (the View Controller) and we need the Models to complete the M and the V and the C
    
    //two constant collections that will store some questions and corresponding answers as strings
    //one variable that will keep track of the user button pressing.
    //I am guessing that eventually, as the book progresses, the models will go into their own classes, and in separate folders. 
    //for now, this is just how it is.
    
    //collection of questions
    
    let questions: [String] =
    [
        "how are you?",
        "where are you from?",
        "how long has it been?"
    ]
    
    //collection of answers
    let answers: [String] =
    [
        "i am good",
        "i am from mysore, the greatest city on earth, yeah!",
        "its been a while man...its been a while"
    ]
    
    //the user button keeping track variable
    var currentQuestionIndex: Int = 0
    
    //alright, we got the question lable to animate itself (appear and disappear gracefully)
    //now we will make it fly out like a crazy person, like to the left and to the right
    //the UI controls move around by many means, but its easier with the constraints. 
    //so we will create some outlets that access the constraints
    
    @IBOutlet var currentQuestionLabelCenterXConstraint: NSLayoutConstraint!
    @IBOutlet var nextQuestionLabelCenterXConstraint: NSLayoutConstraint!
    
    //Okay, I went through the textbook for animating the above constraints. I think I can skip it. 
    //in order to do this, I will have to understand constraints to the hilt, and then come back to this
    //for now, marking this as a TODO
    //TODO - understand constraints like crazy. Then, learn how to animate it
    //TODO - for now, we dont need to animate my apps
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //set the current question lable to the very first question
        currentQuestionLabel.text = questions[currentQuestionIndex]
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        //the animation's purpose is to make the label become visible slowly.
        //before it can be visible, it must be made invisible. set its opacity to 0
        
        //questionLabel.alpha = 0
        
        //we have two question lables now, settin the first one to lower alpha
        nextQuestionLabel.alpha = 0
    }
    
    //the outlets are pointing to the labels. Let connect those buttons to controller, so when the button i clicked, the desired effect takes place.
    
    @IBAction func showNextQuestion(_ sender: UIButton)
    {
        //first, increment the question index variable.
        currentQuestionIndex += 1
        
        //next, if the question index has exceeded the length of the questions collection, then reset it to 0
        //as they say in the movies, we should not exceed our grasp. an array pointer should not exceed its length
        
        if(currentQuestionIndex == questions.count)
        {
            currentQuestionIndex = 0
        }
        
        //push the question from the collection, as pointed at by the current index, into the view.
        let current_question: String = questions[currentQuestionIndex]
        //questionLabel.text = current_question
        
        
        //setting the next question to the next question label
        nextQuestionLabel.text = current_question
        
        //change the answer label to something that is as of yet unknown
        let yet_unknown: String = "???"
        answerLabel.text = yet_unknown
        
        //okay, I have the animation function ready. lets call it.
        animateLabelTransitions()
        
    }

    @IBAction func showNextAnswer(_ sender: UIButton)
    {
        //push the answer from the collection, as pointed at by the current index, into the view.
        let current_answer: String = answers[currentQuestionIndex]
        answerLabel.text = current_answer
    }
    
    //alright, lets do the animation thing.
    
    //here is a closure - a discrete bundle of functionality
    
    func animateLabelTransitions()
    {
        //this would be the animation closure
        
        let animationClosure =
        {
            () -> Void in  //does not return anything, hence the void
            //I am trying to animate the question Label. I start by setting the alpha to 1
            //self.questionLabel.alpha = 1
            //the current question label (which will be 1, gradually animates to 0)
            self.currentQuestionLabel.alpha = 0
            //the next question label which we set to 0, gradually animats to 1
            self.nextQuestionLabel.alpha = 1
            //the alpha - I think - controls the visibility or transparency or opacity of the UI control
        }
        
        
        //the actual animation is being done by this call here to the UIVIEW animation thing
        //the first part is the total duration of the animations in seconds
        //the second part is the actual animation to play out
        //UIView.animate(withDuration: 2, animations: animationClosure)
        
        //modifying this animate call to now include a completion closure along with the animation closure
        //I can use the completion closure to do things when the animation completes.
        //in this case, once the animation is complete, I want the labels to be swapped
        
        UIView.animate(withDuration: 2.0, delay: 0, options: [], animations: animationClosure, completion:
            {
             
                _ in swap(&self.currentQuestionLabel, &self.nextQuestionLabel)
                
            })
        
        //above, note that the second part is the delay before the completion stuff is triggered
        //option is a list of options sent as an array. here, I am sending a blank array
        //then is the previously used animation closure
        //then is the newly added completion closure
        //you will notice that for the animation closure, i used an explicit definition in the beginning of this method definition
        //for the completion closure, i am using the closure directly without any explicit definitio.
        //if required, the completion closure used here can also be defined explictly but i am not doing that right now
        
        //the swap, as the name implese, swaps the two objects. so, as the next question button is pressed, alternating labels are displayed, with each of them losing and gaining their opacity, hence aiding in the animation.
    }
    
    

}


