# quiz2

This is similar to the quiz repo. adds some animation for the question change that is all. 

**references and links**

no specific references that I can talk about. 

**other stuff**

Terms to remember man.

Navigators
Utilities
Interface Builder
Document Outline
Canvas
Inspector
Library
Connections - Outlet and Action

Warnings

This is an universal app project. However, I dont have icons right now for the iPad Pro. So, there will be a warning about the icon, which can be safely ignored since we are not targeting iPad Pro here.

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 